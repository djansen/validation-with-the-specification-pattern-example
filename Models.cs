﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecificationPatternExample
{
    public interface IPerson
    {
        DateTime? BirthDate { get; set; }
    }
    public class BasePerson
    {
        public string TaxId { get; set; }
        public DateTime? BirthDate { get; set; }
    }

    public class Person : BasePerson, IPerson
    {
        public Guid? PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
