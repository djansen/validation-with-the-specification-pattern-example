﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpecificationPatternExample.Validators;

namespace SpecificationPatternExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var validPerson = new Person
            {
                PersonId = Guid.NewGuid(),
                FirstName = "Steve",
                LastName = "Smith",
                TaxId = "999999999",
                BirthDate = DateTime.Parse("1/1/75")
            };

            var invalidPerson = new Person
            {
                PersonId = null,
                FirstName = string.Empty,
                LastName = " ",
                TaxId = "123",
                BirthDate = DateTime.Now.AddYears(1)
            };

            Helper.PrintValidation(validPerson);
            Console.WriteLine();
            Console.ReadKey();

            Helper.PrintValidation(invalidPerson);
            Console.WriteLine();
            Console.ReadKey();
            
            Helper.PrintValidationFactory(validPerson);
            Console.WriteLine();
            Console.ReadKey();
            
            Helper.PrintValidationFactory(invalidPerson);
            Console.ReadKey();
        }
    }

    public class Helper 
    {
        public static void PrintValidation(Person person)
        {
            var personValidator = new PersonValidator();
            Console.WriteLine("Person Validator");
            Console.WriteLine(string.Format("{0} {1} is Valid: {2}", person.FirstName, person.LastName, personValidator.IsValid(person).ToString()));
            foreach (var ruleMessage in personValidator.BrokenRules(person))
            {
                Console.WriteLine(string.Format("Validation Message: {0}", ruleMessage.Message));
            }
        }

        public static void PrintValidationFactory(Person person)
        {
            var factoryDefinitions = new List<FactoryDefinition>{ 
                new FactoryDefinition{
                    FactoryName = "SpecificationPatternExample.Factories.BirthdateNotFutureFactory",
                    Feedback = new SpecificationFeedback(SpecificationSeverity.Error, "Birthdate must not be in the future."),
                },

                new FactoryDefinition{
                    FactoryName = "SpecificationPatternExample.Factories.BirthdateNotFutureFactory",
                    Feedback = new SpecificationFeedback(SpecificationSeverity.Warning, "Birthdate should not be in the future."),
                },

                new FactoryDefinition{
                    FactoryName = "SpecificationPatternExample.Factories.LastNameNotEmptyFactory",
                    Feedback = new SpecificationFeedback(SpecificationSeverity.Error, "Last Name must not be blank."),
                },
                
                new FactoryDefinition{
                    FactoryName = "SpecificationPatternExample.Factories.TaxId9Characters",
                    Feedback = new SpecificationFeedback(SpecificationSeverity.Error, "Tax Id must be nine (9) characters."),
                },
                
                new FactoryDefinition{
                    FactoryName = "SpecificationPatternExample.Factories.FirstNameRequiredFactory",
                    Feedback = new SpecificationFeedback(SpecificationSeverity.Error, "First Name is Required."),
                },
                
                new FactoryDefinition{
                    FactoryName = "SpecificationPatternExample.Factories.BirthdateRequiredFactory",
                    Feedback = new SpecificationFeedback(SpecificationSeverity.Error, "Birthdate is Required."),
                },
            };

            var personValidator = new PersonFactoryValidator(factoryDefinitions);
            Console.WriteLine("Person Factory Validator");
            Console.WriteLine(string.Format("{0} {1} is Valid: {2}", person.FirstName, person.LastName, personValidator.IsValid(person).ToString()));
            foreach (var feedbackMessage in personValidator.BrokenRules(person).OrderByDescending(f => f.Severity))
            {
                Console.WriteLine(string.Format("{0}: {1}", feedbackMessage.Severity, feedbackMessage.Message));
            }
        }
    }
}
