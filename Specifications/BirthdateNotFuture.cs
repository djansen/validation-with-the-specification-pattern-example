﻿using SpecificationPatternExample.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecificationPatternExample.Specifications
{
    public class BirthdateNotFuture<T> : ISpecification<T>
        where T : IPerson
    {
        public BirthdateNotFuture(SpecificationFeedback feedback)
        {
            Feedback = feedback;
        }

        public SpecificationFeedback Feedback { get; set; }

        public bool IsSatisfiedBy(T entity)
        {
            return entity.BirthDate <= DateTime.Now;
        }

        
    }
}
