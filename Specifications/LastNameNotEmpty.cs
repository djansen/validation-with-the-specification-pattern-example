﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecificationPatternExample.Specifications
{
    public class LastNameNotEmpty : ISpecification<Person>
    {
        public LastNameNotEmpty(SpecificationFeedback feedback)
        {
            Feedback = feedback;
        }

        public SpecificationFeedback Feedback { get; set; }

        public bool IsSatisfiedBy(Person entity)
        {
            return !string.IsNullOrWhiteSpace(entity.LastName);
        }
    }
}
