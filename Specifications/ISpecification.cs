﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecificationPatternExample.Specifications
{
    public interface ISpecification<T>
    {
        SpecificationFeedback Feedback { get; set; }

        bool IsSatisfiedBy(T entity);
    }
}
