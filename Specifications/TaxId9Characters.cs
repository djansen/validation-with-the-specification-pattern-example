﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecificationPatternExample.Specifications
{
    public class TaxId9Characters<T> : ISpecification<T>
            where T : BasePerson
    {
        public TaxId9Characters(SpecificationFeedback feedback)
        {
            Feedback = feedback;
        }

        public SpecificationFeedback Feedback { get; set; }
        public bool IsSatisfiedBy(T entity)
        {
            return entity.TaxId.Length == 9;
        }
    }
}
