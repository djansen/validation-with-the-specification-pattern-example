﻿using SpecificationPatternExample.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SpecificationPatternExample.Validators
{
    public class PersonValidator : IValidator<Person>
    {
        private readonly IList<ISpecification<Person>> _rules =
            new List<ISpecification<Person>>
            {
                new BirthdateNotFuture<Person>(new SpecificationFeedback(SpecificationSeverity.Error, "Birthdate must not be in the future.")),
                new LastNameNotEmpty(new SpecificationFeedback(SpecificationSeverity.Error, "Last Name must not be empty.")),
                new TaxId9Characters<Person>(new SpecificationFeedback(SpecificationSeverity.Error, "Tax Id must be nine characters.")),
                new RequiredFieldValdator<Person, string>(p => p.FirstName, new SpecificationFeedback(SpecificationSeverity.Error, "First Name is Required.")),
                new RequiredFieldValdator<Person, DateTime?>(p=> p.BirthDate, new SpecificationFeedback(SpecificationSeverity.Error, "Birth Date is Required."))
            };
        public bool IsValid(Person entity)
        {
            return !BrokenRules(entity).Any();
        }
        public IEnumerable<SpecificationFeedback> BrokenRules(Person entity)
        {
            return _rules.Where(rule => !rule.IsSatisfiedBy(entity))
                .Select(rule => rule.Feedback);
        }
    }
}
