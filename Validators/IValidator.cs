﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecificationPatternExample.Specifications
{
    public interface IValidator<T>
    {
        bool IsValid(T entity);
        IEnumerable<SpecificationFeedback> BrokenRules(T entity);
    }
}
