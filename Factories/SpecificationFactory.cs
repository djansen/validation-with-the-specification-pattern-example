﻿using SpecificationPatternExample.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SpecificationPatternExample.Factories
{
    public class SpecificationFactory<T>
    {
        public static IEnumerable<ISpecificationFactory<T>> LoadFactory()
        {
            var specifications = new List<ISpecificationFactory<T>>();

            return specifications.AsEnumerable();
        }

        public static ISpecificationFactory<T> LoadFactory(string factoryName)
        {
            return Assembly.GetExecutingAssembly().CreateInstance(factoryName) as ISpecificationFactory<T>;
        }
    }
}
