﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpecificationPatternExample.Specifications;

namespace SpecificationPatternExample.Factories
{
    public interface ISpecificationFactory<T>
    {
        ISpecification<T> CreateSpecification(SpecificationFeedback feedback);
    }
}
