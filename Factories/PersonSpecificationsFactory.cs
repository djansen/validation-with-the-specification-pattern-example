﻿using SpecificationPatternExample.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecificationPatternExample.Factories
{
    public class BirthdateNotFutureFactory : ISpecificationFactory<Person>
    {
        public ISpecification<Person> CreateSpecification(SpecificationFeedback feedback)
        {
            return new BirthdateNotFuture<Person>(feedback);
        }
    }

    public class LastNameNotEmptyFactory : ISpecificationFactory<Person>
    {
        public ISpecification<Person> CreateSpecification(SpecificationFeedback feedback)
        {
            return new LastNameNotEmpty(feedback);
        }
    }

    public class FirstNameRequiredFactory : ISpecificationFactory<Person>
    {
        public ISpecification<Person> CreateSpecification(SpecificationFeedback feedback)
        {
            return new RequiredFieldValdator<Person, string>(p => p.FirstName, feedback);
        }
    }

    public class BirthdateRequiredFactory : ISpecificationFactory<Person>
    {
        public ISpecification<Person> CreateSpecification(SpecificationFeedback feedback)
        {
            return new RequiredFieldValdator<Person, DateTime?>(p => p.BirthDate, feedback);
        }
    }

    public class TaxId9Characters : ISpecificationFactory<Person>
    {
        public ISpecification<Person> CreateSpecification(SpecificationFeedback feedback)
        {
            return new TaxId9Characters<Person>(feedback);
        }
    }
}
